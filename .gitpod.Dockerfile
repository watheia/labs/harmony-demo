FROM gitpod/workspace-full

# Install custom tools, runtimes, etc.
# For example "bastet", a command-line tetris clone:
# RUN brew install bastet
#
# More information: https://www.gitpod.io/docs/config-docker/

# Install ssh and bit dependencies
RUN apt-get update 
RUN apt-get upgrade -y 
RUN apt-get install -y  apt-transport-https gcc make python g++
RUN apt-get install git -y 

ENV PATH="${PATH}:${HOME}/bin"
RUN npm install --global npm yarn prettier eslint jest ts-node @teambit/bvm --unsafe-perm=true
RUN bvm install && \
    bit config set analytics_reporting false && \
    bit config set error_reporting false && \
    bit config set no_warnings true && \
    bit init --harmony && \
    bit install


